// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:uninav/components/drawer.dart';
import 'package:uninav/components/hamburger_menu.dart';
import 'package:uninav/controllers/map_controller.dart';
import 'package:latlong2/latlong.dart';

import 'package:uninav/main.dart';
import 'package:uninav/map.dart';

void main() {
  testWidgets('Drawer Test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    Get.put(MyMapController());
    await tester.pumpWidget(GetMaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Test'),
          // start hamburger menu to open drawer
          leading: const HamburgerMenu(),
        ),
        drawer: MyDrawer(),
      ),
    ));

    // Verify that our counter starts at 0.
    expect(find.byIcon(Icons.menu), findsOne);
    expect(find.byIcon(Icons.settings), findsNothing);
    expect(find.byIcon(Icons.map), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.menu));
    await tester.pump(const Duration(seconds: 1));

    // Verify that our counter has incremented.
    expect(find.byIcon(Icons.settings), findsOne);
    expect(find.byIcon(Icons.map), findsOne);
  });
}
